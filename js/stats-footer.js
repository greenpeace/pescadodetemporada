/* jshint browser: true, esversion: 6 */
/* global cookieTrackingManager, console, jQuery, _hsq, gtag, fbq, obApi, googleTrackingConfig, cookieManageUI, hyperSegments, dynamicSegmentation */

const trackingScripts = {

    /**
     * Propriety to check if the tracking has initialized.
     */
    hasInitialized: false,
    
    /**
     * Runs all the tracking scripts checking permissions
     */
    initAll: function () {
    
        if (cookieTrackingManager.canItrack("analytics")) {
            gtag('consent', 'update', {'analytics_storage': 'granted'});        
            this.googleAnalyticsFooter();
            this.googleTagManager();
            this.hotjar();
        } else {
            this.googleAnalyticsFooter();
        }

        if (cookieTrackingManager.canItrack("segmentation")) {
            // this.oneSignalZ();
        }
        
        if (cookieTrackingManager.canItrack("advertisement")) {
            gtag('consent', 'update', {'ad_storage': 'granted'});
            this.facebook();
            this.twitter();
            this.outbrain();
            this.taboola();
            this.adgravityPageView();
        }
        
        if (cookieTrackingManager.canItrack("segmentation") && cookieTrackingManager.canItrack("advertisement") ) {
            this.hubspot();
        }

        this.hasInitialized = true;

    },
    
    
    // ----------- Analytics ----------- 

    /**
     * Google Analytics, footer part
     */
    googleAnalyticsFooter: function () {

        if ( typeof gtag === "function") {
            if (cookieTrackingManager.canItrack("analytics")) {
                googleTrackingConfig.optimize_id = 'OPT-PB5SW8Q';
                // googleTrackingConfig.cookie_flags = 'SameSite=None;Secure'; // Needs more checking
            }            
            gtag('config', 'G-XRCZP6D29Q', googleTrackingConfig);
        }
        
        setTimeout(function(){
            if (typeof(window.timeSinceDomLoaded) === "number") {
                gtag('event', 'timing_complete', {
                    'name': 'DOMContentLoaded',
                    'value': window.timeSinceDomLoaded,
                    'event_category': 'Loading'
                });            
            }

            if (typeof(window.timeSinceEventLoad) === "number") {
                gtag('event', 'timing_complete', {
                    'name': 'load',
                    'value': window.timeSinceEventLoad,
                    'event_category': 'Loading'
                });
            }    
        }, 5000);
    
    },

    /**
     * Hotjar initialization
     */
    hotjar: function () {
        
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1356277,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');

    },
    
    /**
     * Google Tag Manager initialization
     */
    googleTagManager: function() {
        
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-K9LBN3C');
    
    },

    // ----------- Segmentation ----------- 

    /**
     * Hubspot initalization and pageview
     */
    hubspot: function () {
        
        // Needs <div id="hubspotEmbed"></div> in the html before using this script FIXME
        var t = document.getElementById("hubspotEmbed"),
            e = document.createElement("script");
        e.src = "//js.hs-scripts.com/5361482.js"; 
        e.setAttribute("id", "hs-script-loader"); 
        e.setAttribute("type", "text/javascript"); 
        e.setAttribute("defer", "defer"); 
        e.setAttribute("async", "async"); 
        t.appendChild(e);

    },
    
    /**
     * One Signal User consent
     */
    oneSignalZ: function(){
        
        setTimeout(function(){
            var OneSignal = window.OneSignal || [];
            window.OneSignal.push(function() {
                OneSignal.provideUserConsent(true);
            });                        

        }, 4000);
     
    },

    // ----------- Advertising ----------- 

    /**
     * Adwords is controled by the spanishRemarketing tool FIXME
     */
    
    /**
     * Facebook initialization and pageview
     */
    facebook: function () {

        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1055834218174209'); // De GPI
        fbq('track', "PageView");
        
    },
    
    /**
     * Twitter initialization
     */
    twitter: function () {
        
            // Needs <div id="twitterAds"></div> in the html before using this script FIXME
            var t = document.getElementById("twitterAds"),
                e = document.createElement("script");
            e.src = "//platform.twitter.com/oct.js"; 
            e.setAttribute("defer", "defer"); 
            t.appendChild(e);
        
    },
    
    /**
     * Outbrain initialization and page view
     */
    outbrain: function () {

      !function(_window, _document) {
        var OB_ADV_ID='002d6df58f70160012cc266f46bbd90888';
        if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}
        var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = '1.1';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement('script');tag.async = true;tag.src = '//amplify.outbrain.com/cp/obtp.js';tag.type = 'text/javascript';var script = _document.getElementsByTagName('script')[0];script.parentNode.insertBefore(tag, script);}(window, document);
        obApi('track', 'PAGE_VIEW');
        
    },

    /**
     * Taboola
     */
    taboola: function () {

        const taboola = document.createElement("img");
        taboola.src = "https://trc.taboola.com/1335560/log/3/unip?en=page_view";
        taboola.width = 0;
        taboola.height = 0;
        taboola.style = "display: none;";
        jQuery("footer").append(taboola);
        
    },

    /**
     * Pageview for adgravity.com
     */
    adgravityPageView: function(){
        try {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = '//bucket.cdnwebcloud.com/adg-greenpeace-ret11.js?z=' + Math.random();
            script.async = true;
            script.defer = true;
            document.getElementsByTagName('head')[0].appendChild(script);
        } catch(n_o_outer_inform) {
            console.log('n_o_outer_inform:' + n_o_outer_inform.message);
        }
    }
    
};


/**
 * Initializing tracking
 */
cookieTrackingManager.read();

if (cookieTrackingManager.needToAskConsent() === false) {

    trackingScripts.initAll();
    
} else {
    
    cookieManageUI.open1stBox(window.ABtestCookieVariant);
    
}
