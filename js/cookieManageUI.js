/* jshint browser: true, esversion: 6 */
/* global URLSearchParams, jQuery, cookieTrackingManager, trackingScripts, gtag */

const cookieManageUI = {

    /**
     * Open the initial screen both on the B and C variants. Enables event listners
     * @param {string} nameWariant Name of the AB test variant
     */
    open1stBox: function () {

        jQuery("#firstCookieBox").modal({
            escapeClose: false,
            clickClose: false,
            showClose: false
        });          
 
        jQuery("#firstCookieBox [data-cookieResponse=accept]").on("click", function () {
            cookieManageUI.close1stBox(1);
            cookieTrackingManager.consent.allowAll = true;
            delete(cookieTrackingManager.consent.denyAll);
            cookieTrackingManager.write();
            trackingScripts.initAll();
            gtag('event', 'Click', {
                'event_label': 'Accept',
                'event_category': 'CookiePrivacy',
                'non_interaction': true
            });
        });

        jQuery("#firstCookieBox [data-cookieResponse=config]").on("click", function () {
            cookieManageUI.close1stBox();
            cookieManageUI.openSettingsBox();
            gtag('event', 'Click', {
                'event_label': 'Config',
                'event_category': 'CookiePrivacy',
                'non_interaction': true
            });
        });

        jQuery("#firstCookieBox [data-cookieResponse=check_policy]").on("click", function () {
            gtag('event', 'Click', {
                'event_label': 'Check Policy',
                'event_category': 'CookiePrivacy',
                'non_interaction': true
            });
        });

    },

    close1stBox: function () {

        jQuery.modal.close();

    },

    openSettingsBox: function () {
        
            setTimeout(function () {
                $("#secondCookieBox").modal({
                    escapeClose: false,
                    clickClose: false,
                    showClose: false
                });
            }, 100);

        jQuery("#secondCookieBox [data-cookieResponse=accept_all]").on("click", function () {
            cookieManageUI.closeSettingsBox();
            cookieTrackingManager.consent.allowAll = true;
            delete(cookieTrackingManager.consent.denyAll);
            cookieTrackingManager.write();
            trackingScripts.initAll();
            gtag('event', 'Click', {
                'event_label': 'Accept all',
                'event_category': 'CookiePrivacy',
                'non_interaction': true
            });
        });

        jQuery("#secondCookieBox [data-cookieResponse=deny_all]").on("click", function () {
            cookieManageUI.closeSettingsBox();
            cookieTrackingManager.consent.denyAll = true;
            delete(cookieTrackingManager.consent.allowAll);
            cookieTrackingManager.erraseAll(); 
            // cookieTrackingManager.write(); // Allready in erraseAll()
            trackingScripts.initAll();
            gtag('event', 'Click', {
                'event_label': 'Deny all',
                'event_category': 'CookiePrivacy',
                'non_interaction': true
            });
        });

        jQuery("#secondCookieBox [data-cookieResponse=OK]").on("click", function () {
            cookieManageUI.closeSettingsBox();
            cookieTrackingManager.consent.cats = {};
            cookieTrackingManager.consent.cats.analytics = jQuery("#cookiesAnalitics").prop("checked");
            cookieTrackingManager.consent.cats.segmentation = jQuery("#cookiesSegmentation").prop("checked");
            cookieTrackingManager.consent.cats.advertisement = jQuery("#cookiesAdvertisement").prop("checked");
            cookieTrackingManager.write();
            trackingScripts.initAll();
            gtag('event', 'Click', {
                'event_label': 'OK ' + String(cookieTrackingManager.consent.cats.analytics) + "," + String(cookieTrackingManager.consent.cats.segmentation) + "," + String(cookieTrackingManager.consent.cats.advertisement) ,
                'event_category': 'CookiePrivacy',
                'non_interaction': true
            });
        });

        jQuery("#secondCookieBox [data-cookieResponse=check_policy]").on("click", function () {
            gtag('event', 'Click', {
                'event_label': 'Check Policy Settings' ,
                'event_category': 'CookiePrivacy',
                'non_interaction': true
            });
            
        });

    },

    closeSettingsBox: function () {
        jQuery.modal.close();
    }

};
