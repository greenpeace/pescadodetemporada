/**
 * Sends to Google Analytics information about click events
 * To track a click event add data-click="Whatever_you_want" to the <a> or any other tag.
 * Click to show more information are tracked as well
 * 
 */
(function(){
	
	$("*[data-click]").on("click", function(){
		var clickLabel = $(this).data("click");
		gtag('event', "signup", {
			'event_category': "pescadodetemporada",
			'event_label': clickLabel
		});
	});
	
	$(".info").on("click", function() {
		var specie = $(this).next().find("h3").text();
		if ( !$(this).hasClass("active") ) {
			gtag('event', "expand", {
				'event_category': "pescadodetemporada",
				'event_label': specie
			});
		}
		
	});
	
	$("#HazteSocio").on("click", function() {
		window.open('https://es.greenpeace.org/es/que-puedes-hacer-tu/hazte-socio/?utm_medium=text&utm_source=otro&utm_campaign=Pescado&utm_content=Pescadodetemporada&utm_term=MensageGracias', '_blank')
		if (typeof ga == "function") {
			gtag('event', "click", {
				'event_category': "pescadodetemporada",
				'event_label': "HazteSocio"
			});
		}
	});
	
})();