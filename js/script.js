window.greenpeace = {

}

$(document).ready(function () {
    setTimeout(function () {
        window.greenpeace.grid = $(".grid").masonry({
            itemSelector: ".grid-item",
            gutter: 30
        });
    }, 10);

    function getAncorName() {
        var anchor = window.location.hash;
        setTimeout(function () {
            if ($(anchor).length) {
                var anchorPos = $(anchor).offset().top;
                $(window).scrollTop(anchorPos - 70);
            }
        }, 250);
    }

    getAncorName();

    // $('.checkBox').addClass("active");

    $('.checkBox').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('#checkBox').attr('checked', false);
        } else {
            $(this).addClass('active');
            $('#checkBox').attr('checked', true);
        }
    });

    function setImage() {
        setTimeout(function () {
            var cookerCont = $('.cocinero-image').width();
            $('.cocinero').css({ 'width': cookerCont, 'height': cookerCont });
        }, 200);
    }

    setImage();

    $(window).resize(function () {
        setImage();
    });

    $('nav i').on('click', function () {
        if ($('nav i').hasClass('active')) {

            $('nav i').removeClass('active');
            $('menu').removeClass('active');
            //$('.swiper-container.gallery-thumbs').hide();
            $('.menu').slideUp(function () {
                //$('.swiper-container.gallery-thumbs').show();
                $('nav i').attr('class', 'fa fa-bars');
            });

        } else {

            //$('.swiper-container.gallery-thumbs').hide();
            $('nav i').attr('class', 'fa fa-times active');
            $('.menu').addClass('active');
            $('.menu').slideDown();

        }
    });


    $('.info').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('i').attr('class', 'fa fa-plus-circle');
            $(this).find('span').html('Ampliar información');
            $(this).next('.extra-info').hide();
        } else {
            $(this).addClass('active');
            $(this).find('i').attr('class', 'fa fa-minus-circle');
            $(this).find('span').html('Reducir información');
            $(this).next('.extra-info').show();
        }

        setTimeout(function () {
            if (greenpeace && greenpeace.grid) greenpeace.grid.masonry();
        }, 10);
    });

});
/*
if (window.navigator.standalone) {
    $(document).on(
        "click",
        "a",
        function (event) {

            // Stop the default behavior of the browser, which
            // is to change the URL of the page.
            event.preventDefault();

            // Manually change the location of the page to stay in
            // "Standalone" mode and change the URL at the same time.
            location.href = $(event.target).attr("href");

        }
    );
}*/
