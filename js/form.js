/**
 * Privacy policy link and tab
 */
$("#privacy_policy_link").on("click", function (event) {
    event.preventDefault();
    $("#privacy_policy_text").toggleClass("hidden");
    if ($("#privacy_policy_text").hasClass("hidden")) {
        $("#privacy_policy_text").attr("aria-hidden", "true");
    } else {
        $("#privacy_policy_text").attr("aria-hidden", "false");
    }
    // Google Analytics event
    if (typeof ga == "function") {
        ga('send', 'event', 'pescadodetemporada', 'privacy', 'open');
    }
});
$("#hide_privacy_policy").on("click", function () {
    $('#privacy_policy_text').addClass("hidden");
    // Google Analytics event
    if (typeof ga == "function") {
        ga('send', 'event', 'pescadodetemporada', 'privacy', 'close');
    }
});

/**
 * @namespace UTM values related functions
 */
var utm = {

    /**
	 * Parse URL parameters
	 * @param   {string}   last_source_string String to parse, starting with ?
	 * @returns {object} Parameters and values
	 */
    getJsonFromString: function (last_source_string) {
        var search = last_source_string.substring(1);
        var result = search ? JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
		function (key, value) { return key === "" ? value : decodeURIComponent(value) }) : {}
        return result;
    },

    /**
	 * Put the utm values into the form hidden fields, and pass them to Engaging Networks. If it's a Google Adwords campaign, process gclid and add campaign name
	 * @param {object} x Object with params and values
	 */
    putValuesInForm: function (x) {

        if (x["gclid"]) {
            x["utm_medium"] = "cpc";
            x["utm_source"] = "google";
            x["utm_campaign"] = "pescadodetemporada";
        }

        $("#utm_medium").val(x["utm_medium"]);
        $("#utm_source").val(x["utm_source"]);
        $("#utm_campaign").val(x["utm_campaign"]);
        $("#utm_content").val(x["utm_content"]);
        $("#utm_term").val(x["utm_term"]);

    }

}

var utm_values = utm.getJsonFromString(window.location.search);
utm.putValuesInForm(utm_values);


/**
 * Define error messages (to allow multi language)
 */
var errorMessages = {
    "es": {
        first_name: {
            required: "Tu nombre por favor"
        },
        last_name: {
            required: "Tu apellido por favor"
        },
        email: {
            required: "Tu email por favor",
            email: "Un email válido por favor, ej. nombre@dominio.tld"
        },
        privacy: {
            required: "Falta aceptar la política de privacidad"
        }
    }
}

/**
 * @namespace Custom validation related
 */
var customValidation = {

    /**
	 * Fix email validation in jQuery Validation. Domain names must have a dot.
	 * @param   {string} value Email address
	 * @returns {boolean} Is it a valid email domain (it includes a dot) 
	 */
    isValidEmailDomain: function (value) {
        var x = /@.+\./.test(value);
        return x
    },

    /**
	 * Known str_replace funcion. Required in valida_nif_cif_nie
	 */
    str_replace: function (e, t, n) {
        var r = e, i = t, s = n;
        var o = i instanceof Array, u = s instanceof Array, r = [].concat(r), i = [].concat(i), a = (s = [].concat(s)).length;
        while (j = 0, a--) {
            if (s[a]) {
                while (s[a] = s[a].split(r[j]).join(o ? i[j] || "" : i[0]), ++j in r) { }
            }
        }
        return u ? s : s[0]
    },

    /**
	 * Validates Spanish ID number
	 * @param {string} e DNI, NIE or CIF
	 * @returns {number} 1 = NIF ok, 2 = CIF ok, 3 = NIE ok, -1 = NIF error, -2 = CIF error, -3 = NIE error, 0 = ??? error
	 */
    valida_nif_cif_nie: function (e) { var t = e.toUpperCase(); var r = "TRWAGMYFPDXBNJZSQVHLCKE"; if (t !== "") { if (!/^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$/.test(t) && !/^[T]{1}[A-Z0-9]{8}$/.test(t) && !/^[0-9]{8}[A-Z]{1}$/.test(t)) { return 0 } if (/^[0-9]{8}[A-Z]{1}$/.test(t)) { posicion = e.substring(8, 0) % 23; letra = r.charAt(posicion); var s = t.charAt(8); if (letra == s) { return 1 } else { return -1 } } suma = parseInt(e[2]) + parseInt(e[4]) + parseInt(e[6]); for (i = 1; i < 8; i += 2) { temp1 = 2 * parseInt(e[i]); temp1 += ""; temp1 = temp1.substring(0, 1); temp2 = 2 * parseInt(e[i]); temp2 += ""; temp2 = temp2.substring(1, 2); if (temp2 == "") { temp2 = "0" } suma += parseInt(temp1) + parseInt(temp2) } suma += ""; n = 10 - parseInt(suma.substring(suma.length - 1, suma.length)); if (/^[KLM]{1}/.test(t)) { if (e[8] == String.fromCharCode(64 + n)) { return 1 } else { return -1 } } if (/^[ABCDEFGHJNPQRSUVW]{1}/.test(t)) { t = n + ""; if (e[8] == String.fromCharCode(64 + n) || e[8] == parseInt(t.substring(t.length - 1, t.length))) { return 2 } else { return -2 } } if (/^[T]{1}/.test(t)) { if (e[8] == /^[T]{1}[A-Z0-9]{8}$/.test(t)) { return 3 } else { return -3 } } if (/^[XYZ]{1}/.test(t)) { pos = customValidation.str_replace(["X", "Y", "Z"], ["0", "1", "2"], t).substring(0, 8) % 23; if (e[8] == r.substring(pos, pos + 1)) { return 3 } else { return -3 } } } return 0 }

}

/**
 * Custom JS methods
 */
jQuery.validator.addMethod("es_nifcifnie", function (value, element) {
    return this.optional(element) || customValidation.valida_nif_cif_nie(value.toUpperCase()) >= 1;
}, "Valid Spanish NIF, NIE or CIF for example 82451384H or X6909535J");

jQuery.validator.addMethod("nocif", function (value, element) {
    return this.optional(element) || customValidation.valida_nif_cif_nie(value.toUpperCase()) != 2;
}, "Just DNI or NIE please");

jQuery.validator.addMethod("es_phone", function (value, element) {
    return this.optional(element) || /^[6789]\d{8}$/.test(value) || value == "TelÃ©fono, Ej. 623456789";
}, "Spanish landline or mobile phone, for example 623456789");

jQuery.validator.addMethod("emailDomain", function (value, element) {
    return this.optional(element) || customValidation.isValidEmailDomain(value) == true;
}, "A valid email, please. Ex: name@domain.tld");

/**
 * Form processing. Sends data to the CRMs
 */

window.atempts = 0;

validator = $("#mainform").validate({

    submitHandler: function (form) {

        atempts = atempts + 1;

        if (atempts == 1) {

            // SEND AJAX
            jQuery.ajax({

                beforeSend: function () {
                    // console.log("Antes de enviar");
                },
                url: "https://e-activist.com/ea-action/action",
                dataType: 'jsonp',
                data: {

                    "format": "json",
                    "ea.client.id": $("#ea_client_id").val(),
                    "ea.campaign.id": $("#ea_campaign_id").val(),
                    "ea.form.id": $("#ea_form_id").val(),
                    "ea.submitted.page": "1",
                    "ea_requested_action": "ea_submit_user_form",
                    "ea.AJAX.submit:": "true",

                    "utm_medium": $("#utm_medium").val(),
                    "utm_source": $("#utm_source").val(),
                    "utm_campaign": $("#utm_campaign").val(),
                    "utm_content": $("#utm_content").val(),
                    "utm_term": $("#utm_term").val(),

                    "first_name": $("#first_name").val(),
                    "last_name": $("#last_name").val(),
                    "email": $("#email").val(),
                    "id_number": $("#id_number").val(),
                    "phone_number": $("#phone_number").val(),
                    "privacy": $("#checkBox:checked").val(),

                }

            }).done(function (data) {

                $("#mainform, #form_text").hide();
                $(".formSended").show();

                // Signup - Google Analytics event
                if (typeof ga == "function") {
                    ga('send', 'event', 'pescadodetemporada', 'signup', 'signup');
                }
				
				// Signup - Facebook event ( Advertising )
				if ( typeof(fbq) == "function" ) {
					fbq("track", "CompleteRegistration");
				}
				
				// Signup - Yandex Metrica Signup
				if ( typeof(yaCounter38604725) == "object") {
					yaCounter38604725.reachGoal( "SignupPetition" );
				}

            }).always(function () {

            }).error(function (form) {

                // Error - Google Analytics event
                if (typeof ga == "function") {
                    ga('send', 'event', 'pescadodetemporada', 'error', 'ajax');
                }

                console.error("There was an error sending the form data");

            });

            // END SEND AJAX
        }
    },

    rules: {
        id_number: {
            es_nifcifnie: true,
            nocif: true
        },
        email: {
            emailDomain: true
        },
        phone_number: {
            es_phone: true
        },
        privacy: {
            required: true
        }
    },

    messages: {
        first_name: {
            required: "Tu nombre por favor",
        },
        last_name: {
            required: "Tu primero apellido por favor",
        },
        email: {
            required: "Tu email por favor",
            email: "Un email válido por favor, ej. nombre@dominio.tld",
            emailDomain: "Un email válido por favor, ej. nombre@dominio.tld"
        },
        id_number: {
            required: "Un DNI o NIE españoles (sin guiones y sin espacios)",
            es_nifcifnie: "Un DNI o NIE españoles. Por ejemplo 82451384H o X6909535J (sin guiones y sin espacios)",
            nocif: "Solo DNI o NIE por favor"
        },
        phone_number: {
            required: "Tu número de teléfono por favor",
            es_phone: "Número de un fijo o movil español valido. Ej. 622345679 (sin guiónes y sin espacios)"
        },
        privacy: {
            required: "Falta aceptar la política de privacidad"
        }
    },

        errorPlacement: function(error, element) {
        // error.appendTo('#errordiv');
        if (element.attr("name") == "privacy") {
            error.insertAfter("#iaccept");
        } else {
            error.insertAfter(element);
        }

    }

});
